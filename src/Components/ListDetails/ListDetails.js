import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as TrelloApi from '../../api';
import CardDetails from '../CardDetails/CardDetails';
import DeleteIcon from '@mui/icons-material/Delete';
import {
    Box, Heading, Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
    Input
} from '@chakra-ui/react';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import SaveIcon from '@mui/icons-material/Save';

class ListDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            hasError: false,
            listName: '',
            cardDetail: false,

        }
    }

    createList = () => {
        const { id } = this.props.match.params;
        TrelloApi.createList(id, this.state.listName)
            .then((res) => {
                const newLists = [...this.state.list];
                newLists.push(res);
                this.setState({
                    list: newLists,
                })
            })
    }

    handleDeleteList = (id) => {
        TrelloApi.deleteList(id, true)
            .then((res) => {
                const newLists = this.state.list.filter((list) => {
                    return list.id !== id;
                })
                this.setState({ list: newLists });
            })
    }


    async componentDidMount() {
        const { id } = this.props.match.params;
        await TrelloApi.getList(id)
            .then(list => {
                this.setState({
                    list: list,
                    hasError: false,

                });
            }).catch((err) => {
                this.setState({
                    list: [],
                    hasError: "Failed to load lists",
                });
            })
    }
    render() {
        const { list, cardName } = this.state;
        return (
            <Box display="flex" flexWrap="wrap" backgroundImage="url('https://mixkit.imgix.net/art/85/85-original.png-1000h.png')"
                h="92.5vh"
                bgRepeat="no-repeat"
                bgSize="100%">
                {
                    list.map((list, index) => {
                        return (
                            <Box key={index}>
                                <Box bg="gray.300" w="300px" m={2} mt={10}>
                                    <Box display="flex" justifyContent="space-between">
                                        <Heading as="h6" fontSize={28} ml={2}>{list.name}</Heading><Box color="gray.50" mr={3}><DeleteIcon onClick={(id) => { this.handleDeleteList(list.id) }} /></Box>
                                    </Box>
                                    <Box>
                                        <CardDetails newCard={cardName} listId={list.id} />

                                    </Box>
                                </Box>

                            </Box>
                        )

                    })
                }



                <Box>
                    <Popover placement="bottom" ml="8rem">
                        <PopoverTrigger>
                            <Box
                                h="3rem" bg="skyblue" w="300px" ml="2rem" mt={10} color="black" textAlign="center" border="1px">
                                <Button fontSize={20} mt={1} p="1rem" bg="skyblue" w="18.5rem" color="white"> + Add another list</Button>
                            </Box >
                        </PopoverTrigger>
                        <PopoverContent>
                            <PopoverArrow />
                            <PopoverBody><Input type="text" onChange={e => { this.setState({ listName: e.target.value }) }} placeholder="Enter list title..." /></PopoverBody>
                            <Box >
                                <PopoverBody><Button ml="5rem" onClick={this.createList} bg="#026AA7" color="white">Add list</Button></PopoverBody>
                                <PopoverCloseButton mt="4.2rem" mr="5rem" fontSize={18} />
                            </Box>
                        </PopoverContent>
                    </Popover>
                </Box>

            </Box>
        )
    }
}
export default ListDetails;